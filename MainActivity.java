import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

//记得在manifest.xml中添加访问网络的权限
public class MainActivity extends Activity {
	
	ProgressDialog progressDialog;
	CustomListViewAdapter listViewAdapter;
	ListView listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		listView = (ListView) findViewById(R.id.imageList);
		DownloadTask task = new DownloadTask(this);
		task.execute(new String[]{URL, URL1, URL2});
		
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("In progress...");
		progressDialog.setMessage("Loading...");
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setIndeterminate(false);
		progressDialog.setMax(100);
		progressDialog.setCancelable(true);
		progressDialog.show();
	}
	
	private class DownloadTask extends AsyncTask<String, Integer, List<RowItem> > {
		
		private Activity context;
		List<RowItem> rowItems;
		int taskCount;
		
		public DownloadTask(Activity context) {
			this.context = context;
		}

		@Override
		protected List<RowItem> doInBackground(String... urls) {
			taskCount = urls.length;
			rowItems = new ArrayList<RowItem>();
			Bitmap map = null;
			for (String url : urls) {
				map = downloadImage(url);
				rowItems.add(new RowItem(map));
			}
			return rowItems;
		}
		
		protected void onProgressUpdate(Integer... progress) {
			progressDialog.setProgress(progress[0]);
			if (rowItems != null) {
				progressDialog.setMessage("Loading " + (rowItems.size() + 1) + "/" + taskCount);
			}
		}
		
		@Override
		protected void onPostExecute(List<RowItem> rowItems) {
			listViewAdapter = new CustomListViewAdapter(context, rowItems);
			listView.setAdapter(listViewAdapter);
			progressDialog.dismiss();
		}
		
		private Bitmap downloadImage(String urlString) {
			int count = 0;
			Bitmap bitmap = null;
			
			URL url;
			InputStream in = null;
			BufferedOutputStream out = null;
			
			try {
				url = new URL(urlString);
				URLConnection conn = url.openConnection();
				int lengthOfFile = conn.getContentLength();
				
				in = new BufferedInputStream(url.openStream());
				ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
				out = new BufferedOutputStream(dataStream);
				
				byte[] data = new byte[512];
				long total = 0L;
				while ((count = in.read(data)) != -1) {
					total += count;
					publishProgress((int)((total * 100) / lengthOfFile));
					out.write(data, 0, count);
				}
				out.flush();
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 1;
				
				byte[] bytes = dataStream.toByteArray();
				bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return bitmap;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	//URL地址
	public static final String URL = "http://gdown.baidu.com/data/wisegame/60288c8f92238775/FruitNinja_1809.apk";
	public static final String URL1 ="http://gdown.baidu.com/data/wisegame/60288c8f92238775/FruitNinja_1809.apk";   
	public static final String URL2 = "http://gdown.baidu.com/data/wisegame/60288c8f92238775/FruitNinja_1809.apk";   

}
