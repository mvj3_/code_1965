import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class CustomListViewAdapter extends BaseAdapter {
	
	Context mContext;
	List<RowItem> rowItems;
	
	public CustomListViewAdapter(Context context, List<RowItem> items) {
		this.mContext = context;
		this.rowItems = items;
	}
	
	private class ViewHolder {
		ImageView imageView;
	}

	@Override
	public int getCount() {
		return rowItems.size();
	}

	@Override
	public Object getItem(int position) {
		return rowItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_item, null);
			holder = new ViewHolder();
			holder.imageView = (ImageView) convertView.findViewById(R.id.thumbnail);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		RowItem rowItem = (RowItem) rowItems.get(position);
		holder.imageView.setImageBitmap(rowItem.getBitmap());
		return convertView;
	}

}